import {StorageType, Complex, Execute, nameRAM} from './types';

export interface IMotherboard {
    chooseRAM(name: nameRAM): void;
    mathOperation(operand1: number, action: string, operand2: number): void;
    rememberString(stringToRemember: string): void;
    getResult(n: number): Complex;
}

export interface Iram {
    name: string;
    storage: StorageType;
    setResult(result: Complex, n: number): Execute;
    getResult(n: number): Complex;
}

export interface Icpu {
    chooseMemory(name: nameRAM): void;
    addition(value1: number, value2: number, n: number): void;
    subtraction(value1: number, value2: number, n: number): void;
    multiplication(value1: number, value2: number, n: number): void;
    division(value1: number, value2: number, n: number): void;
    rememberString(stringToRemember: string, n: number): void;
    getResult(n: number): Complex;
}
