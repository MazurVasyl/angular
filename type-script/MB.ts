import {IMotherboard} from './interfases';
import {cpu} from './CPU';
import {Complex, Func, nameRAM} from './types';

class Motherboard implements IMotherboard {

    static counter(): Func {
        let x: number = 0;
        return (): number => ++x;
    }

    static operationNumber = Motherboard.counter();

    chooseRAM(name: nameRAM): void {
        cpu.chooseMemory(name);
    }

    mathOperation(operand1: number, action: string, operand2: number): void {
        const  N: number = Motherboard.operationNumber();

        switch(action) {
            case '+':
                cpu.addition(operand1, operand2, N);
                break;
            case '-':
                cpu.subtraction(operand1, operand2, N);
                break;
            case '*':
                cpu.multiplication(operand1, operand2, N);
                break;
            case '/':
                cpu.division(operand1, operand2, N);
                break;
        }
    }

    rememberString(stringToRemember: string): void {
        const  N: number = Motherboard.operationNumber();
        cpu.rememberString(stringToRemember, N);
    }

    getResult(n: number): Complex {
        return cpu.getResult(n);
    }
}

export const m1 = new Motherboard();
