To start project enter: 
1. "npm start", 
2. "node index.js",
then read the line, it will show you what to do feather:

    Firstly it will be shown:
    ' Choose any AbstractRAM for your computer. Enter "memory" '
    Enter "memory"

    Then will be shown:
    ' Each of these has a different size of memory. To choose
    --> Corsair enter "corsair"
    --> Crucial enter "crucial"
    --> HyperX enter "hyperX"
    enter your choice: '
    Enter one of three variants:
    "corsair"
    "crucial"
    "hyperX"

    Then will be shown:
    ' the computer is off, to turn it on enter "on" '
    Enter "on"

    At last will be shown:
    ' computer is on! Now you can do something :)
    --> to do addition of two numbers enter "add"
    --> to do subtraction of two numbers enter "subtract"
    --> to do multiplication of two numbers enter "multiple"
    --> to do division of two numbers enter "divide"
    --> to remember string enter "remember string"
    --> to get any result enter "result"
    --> to turn off the computer enter "off" '
    Enter one of these command, but note, that command "result" gives nothing while any command above was not done and command "off" is doing exit from environment.


