"use strict";
exports.__esModule = true;
exports.m1 = void 0;
var CPU_1 = require("./CPU");
var Motherboard = /** @class */ (function () {
    function Motherboard() {
    }
    Motherboard.counter = function () {
        var x = 0;
        return function () { return ++x; };
    };
    Motherboard.prototype.chooseRAM = function (name) {
        CPU_1.cpu.chooseMemory(name);
    };
    Motherboard.prototype.mathOperation = function (operand1, action, operand2) {
        var N = Motherboard.operationNumber();
        switch (action) {
            case '+':
                CPU_1.cpu.addition(operand1, operand2, N);
                break;
            case '-':
                CPU_1.cpu.subtraction(operand1, operand2, N);
                break;
            case '*':
                CPU_1.cpu.multiplication(operand1, operand2, N);
                break;
            case '/':
                CPU_1.cpu.division(operand1, operand2, N);
                break;
        }
    };
    Motherboard.prototype.rememberString = function (stringToRemember) {
        var N = Motherboard.operationNumber();
        CPU_1.cpu.rememberString(stringToRemember, N);
    };
    Motherboard.prototype.getResult = function (n) {
        return CPU_1.cpu.getResult(n);
    };
    Motherboard.operationNumber = Motherboard.counter();
    return Motherboard;
}());
exports.m1 = new Motherboard();
