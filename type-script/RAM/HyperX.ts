import {Complex, Execute, StorageBoolean, StorageNumber, StorageString, StorageType} from '../types';
import AbstractRAM from '../abstractRAM';

export default class HyperX extends AbstractRAM implements AbstractRAM{
    name: string = 'HyperX';
    size: number = 10;

    storage: StorageType = [[], [], []];

    setResult(result: Complex, n: number): Execute {
        if (n > this.size) {
            console.log('RAM is full');
            return 'not executed';
        }

        switch(typeof result) {
            case 'string':
                this.storage[0].push([result, n]);
                console.log(this.storage);
                return 'executed';
            case 'number':
                this.storage[1].push([result, n]);
                console.log(this.storage);
                return 'executed';
            case 'boolean':
                this.storage[2].push([result, n]);
                console.log(this.storage);
                return 'executed';
        }
    }

    getResult(n: number): Complex {
        const result1: StorageString[] = this.storage[0].filter(value => value[1] === n);
        const result2: StorageNumber[] = this.storage[1].filter(value => value[1] === n);
        const result3: StorageBoolean[] = this.storage[2].filter(value => value[1] === n);

        const res: number = 1;

        switch (res) {
            case result1.length:
                return `the result of number ${n} is: ${result1[0][0]}`;
            case result2.length:
                return `the result of number ${n} is: ${result2[0][0]}`;
            case result3.length:
                return `the result of number ${n} is: ${result3[0][0]}`;
        }
    }
}
