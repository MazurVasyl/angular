import Corsair from './RAM/Corsair';
import Crucial from './RAM/Crucial';
import HyperX from './RAM/HyperX';
import {Icpu} from './interfases';
import {ClassName, Complex, nameRAM} from './types';

let ram: ClassName;

export class CPU implements Icpu {
    chooseMemory(name: nameRAM): void {
        switch (name) {
            case nameRAM.Corsair: {
                ram = new Corsair();
                break;
            }
            case nameRAM.Crucial: {
                ram = new Crucial();
                break;
            }
            case nameRAM.HyperX: {
                ram = new HyperX();
                break;
            }
        }
    }

    addition(value1: number, value2: number, n: number): void {
        const result: number = value1 + value2;
        if (ram.setResult(result, n) === 'not executed') {
            console.log('addition is not solved');
        }
        else {
            console.log('addition is solved');
        }
    }

    subtraction(value1: number, value2: number, n: number): void {
        const result: number = value1 - value2;
        if (ram.setResult(result, n) === 'not executed') {
            console.log('subtraction is not solved');
        }
        else {
            console.log('subtraction is solved');
        }
    }

    multiplication(value1: number, value2: number, n: number): void {
        const result: number = value1 * value2;
        if (ram.setResult(result, n) === 'not executed') {
            console.log('multiplication is not solved');
        }
        else {
            console.log('multiplication is solved');
        }
    }

    division(value1: number, value2: number, n: number): void {
        const result: number = value1 / value2;
        if (ram.setResult(result, n) === 'not executed') {
            console.log('division is not solved');
        }
        else {
            console.log('division is solved');
        }
    }

    rememberString(stringToRemember: string, n: number): void {
        console.log('string is remembered');
        ram.setResult(stringToRemember, n);
    }

    getResult(n: number): Complex {
        return ram.getResult(n);
    }
}

export const cpu = new CPU();
