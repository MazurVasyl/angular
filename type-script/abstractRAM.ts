import {Iram} from './interfases';
import {StorageType, Complex, Execute} from './types';

export default abstract class AbstractRAM implements Iram {
    name: string;
    storage: StorageType;

    abstract setResult(result: Complex, n: number): Execute;
    abstract getResult(n: number): Complex;
}
