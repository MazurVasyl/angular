import {m1} from './MB';
import {nameRAM} from './types';
let readline = require('readline');
let Emitter = require('events');

process.on('SIGTERM', () => process.exit(0));

let n1: number = null;
let n2: number = null;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('Choose any RAM for your computer. Enter "memory"');

const EventEmitter = new Emitter();

const workProcess = () => {
    console.log('the computer is off, to tern it on enter "on"');

    EventEmitter.once('on', () => {
        console.log('computer is on! Now you can do something :)');
        console.log('--> to do addition of two numbers enter "add"');
        console.log('--> to do subtraction of two numbers enter "subtract"');
        console.log('--> to do multiplication of two numbers enter "multiple"');
        console.log('--> to do division of two numbers enter "divide"');
        console.log('--> to remember string enter "remember string"');
        console.log('--> to get any result enter "result"');
        console.log('--> to tern off the computer enter "off"');

        EventEmitter.on('add', () => {
            rl.question('enter first number:', (answer1) => {
                n1 = Number(answer1);
                rl.question('enter second number:', (answer2) => {
                    n2 = Number(answer2);
                    m1.mathOperation(n1, '+', n2);
                });
            });
        });

        EventEmitter.on('subtract', () => {
            rl.question('enter first number:', (answer1) => {
                n1 = Number(answer1);
                rl.question('enter second number:', (answer2) => {
                    n2 = Number(answer2);
                    m1.mathOperation(n1, '-', n2);
                });
            });
        });

        EventEmitter.on('multiple', () => {
            rl.question('enter first number:', (answer1) => {
                n1 = Number(answer1);
                rl.question('enter second number:', (answer2) => {
                    n2 = Number(answer2);
                    m1.mathOperation(n1, '*', n2);
                });
            });
        });

        EventEmitter.on('divide', () => {
            rl.question('enter first number:', (answer1) => {
                n1 = Number(answer1);
                rl.question('enter second number:', (answer2) => {
                    n2 = Number(answer2);
                    m1.mathOperation(n1, '/', n2);
                });
            });
        });

        EventEmitter.on('remember string', () =>
            rl.question('enter string:', (answer) => m1.rememberString(answer))
        );

        EventEmitter.on('result', () => {
            rl.question('enter the number of result:', (answer) => {
                console.log(m1.getResult(Number(answer)));
            });
        });

        EventEmitter.on('off', () => {
            console.log('computer is off');
            process.kill(process.pid, 'SIGTERM');
        });
    });
}

EventEmitter.once('corsair', () => {
    m1.chooseRAM(nameRAM.Corsair);
    workProcess();
});

EventEmitter.once('crucial', () => {
    m1.chooseRAM(nameRAM.Crucial);
    workProcess();
});

EventEmitter.once('hyperX', () => {
    m1.chooseRAM(nameRAM.HyperX);
    workProcess();
});

EventEmitter.once('memory', () => {
    console.log('Each of these has a different size of memory. To choose');
    console.log('--> Corsair enter "corsair"');
    console.log('--> Crucial enter "crucial"');
    console.log('--> HyperX enter "hyperX"');

    rl.question('enter your choice:', (answer) => {
        switch (answer) {
            case 'corsair':
                EventEmitter.emit('corsair');
                break;
            case 'crucial':
                EventEmitter.emit('crucial');
                break;
            case 'hyperX':
                EventEmitter.emit('hyperX');
                break;
        }
    });
});

rl.on('line', input => {

    switch (input) {
        case 'memory':
            EventEmitter.emit('memory');
            break;
        case 'on':
            EventEmitter.emit('on');
            break;
        case 'off':
            EventEmitter.emit('off');
            break;
        case 'add':
            EventEmitter.emit('add');
            break;
        case 'subtract':
            EventEmitter.emit('subtract');
            break;
        case 'multiple':
            EventEmitter.emit('multiple');
            break;
        case 'divide':
            EventEmitter.emit('divide');
            break;
        case 'remember string':
            EventEmitter.emit('remember string');
            break;
        case 'result':
            EventEmitter.emit('result');
            break;
    }
});


