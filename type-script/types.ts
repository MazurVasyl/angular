import Corsair from './RAM/Corsair';
import Crucial from './RAM/Crucial';
import HyperX from './RAM/HyperX';

export type StorageType = [StorageString[], StorageNumber[], StorageBoolean[]];

export type StorageString = [string, number];
export type StorageNumber = [number, number];
export type StorageBoolean = [boolean, number];

export type Func = () => number;

export type Complex = string | number | boolean;

export type Execute = 'not executed' | 'executed';

export enum nameRAM {
    Corsair = 'corsair',
    Crucial = 'crucial',
    HyperX = 'hyperX'
}

export type ClassName = Crucial | Corsair | HyperX;
